package arrays1;
import java.util.*;

public class ArraysTest {
    public static void main(String[] args) {
        String word = "Hello";
        String word2 = "World";
        MyArray <String> myAr = new MyArray<>();
        myAr.add(word);
        myAr.add(word2);

        System.out.println(myAr.get(0) + " " + myAr.get(1));
        System.out.println("Performance test");
        long startTime1 = System.nanoTime();
        MyArray <String> myTest = new MyArray<>();
        for (int i = 0; i<10000; i++){
            myTest.add("Word");
        }
        long endTime1 = System.nanoTime();
        long duration = (endTime1 - startTime1);

        long startTime2 = System.nanoTime();
        ArrayList<String> myTest2 =  new ArrayList<String>();
        for (int i = 0; i<10000; i++){
            myTest.add("Word");
        }
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2);

        System.out.println("Duration of executing of adding of 10000 String elements of My Array is: " + duration);
        System.out.println("Duration of executing of adding of 10000 String elements of ArrayList is: " + duration2);


        System.out.println("Testing Pair class:");

        String [] capitals = {"Kiev", "Paris", "Monaco", "Berlin", "Praga", "Sophia", "Lima", "Oslo", "Vachington", "Ottava"};
        String [] country = {"Ukraine", "Peru", "Cuba", "Usa", "Serbia", "France", "Marocco", "Grece", "Iceland", "Check"};

        ArrayList <Pair> aList = new ArrayList<>();
        Pair [] simpleAr = new Pair[5] ;
        Random rand = new Random();
        for (int i =0; i<5;i++){
            String cap = capitals[rand.nextInt(10)];
            String count = country [rand.nextInt(10)];
            aList.add(new Pair(cap,count));
            simpleAr [i]= new Pair(cap,count);
        }
        System.out.println("ArrayList before sorting with compareTo: ");
        System.out.println(aList);
        Collections.sort(aList);
        System.out.println("ArrayList after sorting with compareTo: ");
        System.out.println(aList);

        System.out.println("Array before sorting with compareTo: ");
        for (Pair i : simpleAr) {
            System.out.print(i);
        }
        Arrays.sort(simpleAr);
        System.out.println("Array after sorting with compareTo: ");
        for (Pair i : simpleAr) {
            System.out.print(i);
        }


        Comparator<Pair> myComparator = (o1, o2) -> o1.getCountry().compareTo(o2.getCountry());

        ArrayList <Pair> aList2 = new ArrayList<>();
        Pair [] simpleAr2 = new Pair[5] ;
        Random rand2 = new Random();
        for (int i =0; i<5;i++){
            String cap = capitals[rand2.nextInt(10)];
            String count = country [rand2.nextInt(10)];
            aList2.add(new Pair(cap,count));
            simpleAr2 [i]= new Pair(cap,count);
        }
        System.out.println("ArrayList before sorting with Comparator: ");
        System.out.println(aList2);
        aList2.sort(myComparator);
        System.out.println("ArrayList after sorting with Comparator: ");
        System.out.println(aList2);

        System.out.println("Array before sorting with Comparator: ");
        for (Pair i : simpleAr2) {
            System.out.print(i);
        }
        Arrays.sort(simpleAr2,myComparator);
        System.out.println("Array after sorting with Comparator: ");
        for (Pair i : simpleAr2) {
            System.out.print(i);
        }
        aList.add(new Pair("Amsterdam", "Holland"));
        aList.sort(myComparator);
        Pair searchedItem =  new Pair("Amsterdam", "Holland");

        int position = Collections.binarySearch(aList,searchedItem,myComparator);
        System.out.println("Position of searched element: " + position);
        System.out.println(aList.get(position));






    }




}
