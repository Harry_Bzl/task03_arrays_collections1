package arrays1;

import java.util.Objects;

public class Pair implements Comparable <Pair> {
    private String capital;
    private String country;

    public Pair(String capital, String country) {
        this.capital = capital;
        this.country = country;
    }

    public String getCapital() {
        return capital;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "capital='" + capital + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    @Override
    public int compareTo(Pair otherPair) {
        return this.capital.compareTo(otherPair.capital);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pair)) return false;
        Pair pair = (Pair) o;
        return Objects.equals(getCapital(), pair.getCapital()) &&
                Objects.equals(getCountry(), pair.getCountry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCapital(), getCountry());
    }
}
