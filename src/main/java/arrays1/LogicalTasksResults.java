package arrays1;

import java.util.Arrays;
import java.util.Random;

public class LogicalTasksResults {

    public static void main(String[] args) {
        Random rand = new Random();
        int [] array1 = new int [10];
        int [] array2 = new int [10];


        for (int i=0; i<10;i++ ){
            array1 [i] = rand.nextInt(10);
            array2 [i] = rand.nextInt(10);
        }

        System.out.println("Array 1 is: ");
        LogicalTasksMethods.printArray(array1);

        System.out.println("Array 2 is: ");
        LogicalTasksMethods.printArray(array2);

        System.out.println("Similar num are: ");
        LogicalTasksMethods.printArray(LogicalTasksMethods.findSimilarNumInArrays(array1,array2));

        System.out.println("Diferent num are: ");
        LogicalTasksMethods.printArray(LogicalTasksMethods.findDifferentNumInArrays(array1,array2));


        rand = new Random();
        int [] array3 = new int [20];


        for (int i=0; i<array3.length;i++ ){
            array3 [i] = rand.nextInt(10);

        }

        System.out.println("Array befoure searching repeat is");
        LogicalTasksMethods.printArray(array3);
        System.out.println();
        System.out.println("Array after searching and delete repeat numbers is");
        LogicalTasksMethods.printArray(LogicalTasksMethods.deleteRepeatedElements(array3,2));
        System.out.println();

        System.out.println("Array befoure delete double elements in groop");
        Arrays.sort(array3);
        LogicalTasksMethods.printArray(array3);
        System.out.println();
        System.out.println("Array after delete double elements in groop");
        LogicalTasksMethods.printArray(LogicalTasksMethods.deleteDublicats(array3));
    }
}
