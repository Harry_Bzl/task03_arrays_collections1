package arrays1;

import java.util.ArrayList;
import java.util.HashMap;

public class LogicalTasksMethods {
    public static void printArray (int[] array){
        for (int i:array){
            System.out.print(i+ ", ");
        }
        System.out.println();
    }

    public static boolean contains (int [] array, int element){
        if (array.length==0){
            return  false;
        }
        for (int i:array){
            if (i == element){
                return true;
            }
        }
        return false;

    }

    /**
     * method for finding similar number without using set
     * @param array1
     * @param array2
     * @return array of similar nunbers
     */
    public static int [] findSimilarNumInArrays (int [] array1, int [] array2){
        //Set<Integer> sym = new HashSet<>();
        int [] result1 = new int[array1.length];
        int count = 0;
        boolean flag = false;
        for (int i:array1){
            for (int j: array2) {
                // check for "0" in arrays
                if (i==0 && flag==false && contains(array2,i)){
                    result1 [count] = i;
                    flag=true;
                    count++;
                }
                if (i==j && !contains(result1,i)){
                    result1 [count]=i;
                    count++;
                }
            }
        }
        int [] result2 = new int [count];

        //delete empty cells in array
        for (int i = 0; i < count; i++){
            result2 [i] = result1 [i];
        }
        return result2;

    }
    /**
     * method for finding different number without using set
     * @param array1
     * @param array2
     * @return array of similar nunbers
     */
    public static int [] findDifferentNumInArrays (int [] array1, int [] array2){
        //Set<Integer> sym = new HashSet<>();
        int [] result1 = new int[array1.length+array2.length];
        int count = 0;
        boolean flag = false;
        for (int i:array1){
            // check for "0" in arrays
            if (i==0 && flag==false && !contains(array2,i)){
                result1 [count] = i;
                flag=true;
                count++;
            }
            else if (!contains(array2,i) && !contains(result1,i)){
                result1 [count]=i;
                count++;
            }
        }
        for (int i:array2){
            // check for "0" in arrays
            if (i==0 && !flag && !contains(array1,i)){
                result1 [count] = i;
                flag=true;
                count++;
            }
            else if (!contains(array1,i) && !contains(result1,i)){
                result1 [count]=i;
                count++;
            }
        }
        int [] result2 = new int [count];
        //delete empty cells in array
        for (int i = 0; i < count; i++){
            result2 [i] = result1 [i];
        }
        return result2;
    }

    public static int [] deleteRepeatedElements (int [] array, int numberOfRepeate){
        HashMap <Integer,Integer> counter = new HashMap<>();
        ArrayList <Integer> coef = new ArrayList<>();


        for (int i=0; i< array.length; i++){
            if (!counter.containsKey(array [i])){
                counter.put(array[i],1);
            }
            else  if (counter.get(array[i])<numberOfRepeate){
                counter.put(array[i],counter.get(array[i])+1);
            }
            else {
                coef.add(i);
            }
        }
        int [] workArray = new int [array.length-coef.size()];
        int counter1 = 0;
        // delete repeated elements

        for (int i =0; i<array.length; i++){
            if (!coef.contains(i)){
                workArray [counter1]=array[i];
                counter1++;
            }
        }
        return workArray;

    }

    public static int[] deleteDublicats (int [] array){
        if (array.equals(null)||array.length==0){
            return new int [0];
        }
        int [] workArray = new int [array.length];
        workArray [0] = array [0];
        int count = 1;
        for (int i = 1; i<array.length; i++){
            if (array[i]!=array[i-1]){
                workArray [count]= array[i];
                count++;
            }
        }
        int [] result = new int [count];
        // delete empty cells
        for (int i = 0; i<count;i++){
            result [i] = workArray [i];
        }
        return result;
    }
}
