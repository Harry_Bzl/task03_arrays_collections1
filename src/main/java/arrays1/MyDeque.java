package arrays1;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class MyDeque < Item > implements Iterable < Item > {

    private Node<Item> front;
    private Node<Item> back;
    private int numberOfItems;

    private class Node<Item> {
        Item item;
        Node<Item> next;
        Node<Item> prev;
    }

    public MyDeque() {
        front = null;
        back = null;
        numberOfItems = 0;
    }

    public boolean isEmpty() {
        return (numberOfItems == 0);
    }

    public int size() {
        return numberOfItems;
    }

    public void addFirst(Item item) {
        if (item == null) {
            // When a null element is entered
            throw new java.lang.NullPointerException("Item cannot be null");
        }
        Node<Item> newnode = new Node<Item>();
        newnode.item = item;
        if (numberOfItems == 0) {
            // When there are no elements
            front = newnode;
            back = newnode;
        } else {
            // When there are >=1 elements
            newnode.prev = front;
            newnode.next = null;
            front.next = newnode;
            front = newnode;

        }
        numberOfItems++;
    }

    public void addLast(Item item) {
        if (item == null) {
            // When a null element is entered
            throw new java.lang.NullPointerException("Item cannot be null");
        }
        Node<Item> newnode = new Node<Item>();
        newnode.item = item;
        if (numberOfItems == 0) {
            // When there are no elements
            front = newnode;
            back = newnode;
        } else {
            // When there are >=1 elements
            back.next = newnode;
            newnode.prev = back;
            back = newnode;
        }
        numberOfItems++;
    }

    public Item removeFirst() {
        if (numberOfItems == 0) {
            // When the deque is empty
            throw new NoSuchElementException("No item to remove");
        }
        Item oldfirst = front.item;
        if (numberOfItems == 1) {
            front = null;
            back = null;
        } else {
            front = front.prev;
        }
        numberOfItems--;
        return oldfirst;
    }

    public Item removeLast() {
        if (numberOfItems == 0) {
            // When deque is empty
            throw new NoSuchElementException("No item to remove");
        }
        Item oldlast = back.item;
        if (numberOfItems == 1) {
            front = null;
            back = null;
        } else {
            back = back.next;
        }
        numberOfItems--;
        return oldlast;
    }

    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item> {
        private Node<Item> current = front;

        public boolean hasNext() {
            return (current != null);
        }

        public void remove() {
            throw new UnsupportedOperationException("remove is unsupported");
        }

        public Item next() {
            Item item = current.item;
            current = current.prev;
            return item;
        }
    }

    public static void main(String[] args) {
        MyDeque <String> myDk = new MyDeque <> ();
        String first = "First";
        String last = "Last";

        myDk.addFirst(first);
        System.out.println(myDk.removeFirst());
        myDk.addLast(last);
        System.out.println(myDk.removeLast());


    }
}

